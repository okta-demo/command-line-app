package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strings"
)

func error401(w http.ResponseWriter) {
	w.WriteHeader(401)
	w.Write([]byte("Unauthorized\n"))

}

func main() {
	http.HandleFunc("/token", token)
	http.HandleFunc("/userinfo", userinfo)

	http.ListenAndServe(":8090", nil)
}

func token(w http.ResponseWriter, req *http.Request) {

	user, pass, ok := req.BasicAuth()
	if !ok {
		error401(w)
		return
	}
	accessToken := test(user, pass)
	if accessToken == "-1" {
		error401(w)
		return
	}
	fmt.Fprintf(w, accessToken+"\n")
}

func userinfo(w http.ResponseWriter, req *http.Request) {
	auth := req.Header.Get("Authorization")
	split := strings.Split(auth, "Bearer ")
	fullName := introspect(split[1])
	fmt.Fprintf(w, "\n"+fullName+"\n")
}

func introspect(accessToken string) string {
	resp, err := http.PostForm("https://dev-100661.okta.com/oauth2/default/v1/introspect", url.Values{
		"token":           {accessToken},
		"token_type_hint": {"access_token"},
		"client_id":       {"########################"},
		"client_secret":   {"#####################################"},
	})
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		return "Invalid Token"
	}
	var raw map[string]interface{}
	err = json.Unmarshal(body, &raw)
	if err != nil {
		return "Invalid Token"
	}
	at, ok := raw["fullName"].(string)
	if !ok {

		fmt.Printf("%s\n", "aaila")
		return "Invalid Token"
	}
	return string(at)
}

func test(user, pass string) string {
	resp, err := http.PostForm("https://dev-100661.okta.com/oauth2/default/v1/token", url.Values{
		"grant_type":    {"password"},
		"username":      {user},
		"password":      {pass},
		"client_id":     {"##################################"},
		"client_secret": {"######################################"},
		"scope":         {"openid"},
	})

	//"username":      {"g.opalkpanda@gmail.com"},
	//"password":      {"Test#000"},

	if err != nil {
		return "-1"
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		return "-1"
	}
	var raw map[string]interface{}
	err = json.Unmarshal(body, &raw)
	if err != nil {
		return "-1"
	}
	req, err := http.NewRequest("POST", "https://dev-100661.okta.com/oauth2/default/v1/userinfo", nil)
	at, ok := raw["access_token"].(string)
	if !ok {
		return "-1"
	}
	//fmt.Println(at)
	req.Header.Add("Authorization", "Bearer "+at)
	response, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Println("No error")
		return "-1"
	}
	//fmt.Print(response)
	defer response.Body.Close()
	body, err = ioutil.ReadAll(response.Body)
	if err != nil {
		fmt.Println("N11o error")
		return "-1"
	}
	fmt.Printf("%s\n", string(body))
	fmt.Println("_____________________________________________________")
	fmt.Println("                    Introspect")
	fmt.Println("_____________________________________________________")
	//introspect(at)
	return at
}
